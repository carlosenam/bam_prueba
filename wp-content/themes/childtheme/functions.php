<?php


function parent_theme_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'cta-style', get_stylesheet_directory_uri() . '/css/cta.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( 'parent-style' )
    );
}
add_action( 'wp_enqueue_scripts', 'parent_theme_styles' );

add_shortcode('cta', 'create_cta');

function create_cta($atts){
	$title = $atts['title'] ? $atts['title']:'Our NFL Pick: Vikings +3';
	$remaining = $atts['remaining'] ? $atts['remaining']:'2018/09/20 14:25:00';
	ob_start();
	$days = 'cta_days_'.uniqid();
	$hours = 'cta_hours_'.uniqid();
	$minutes = 'cta_minutes_'.uniqid();
	$seconds = 'cta_seconds_'.uniqid();
	?>
	<div class="cta">
		<div class="cta_image">
			<img src="<?=get_stylesheet_directory_uri()?>/images/cta_image.jpg" alt="Call to Action">
		</div>
		<div class="cta_messages">
			<div class="countdown_cont">
				<div class="countdown">
					<div class="time_box">
						<p class="text">DAYS</p>
						<p class="number" id="<?=$days?>"></p>						
					</div>
					<div class="time_box">
						<p class="text">HOURS</p>
						<p class="number" id="<?=$hours?>"></p>						
					</div>
					<div class="time_box">
						<p class="text">MIN</p>
						<p class="number" id="<?=$minutes?>" ></p>						
					</div>
					<div class="time_box">
						<p class="text">SEC</p>
						<p class="number" id="<?=$seconds?>"></p>						
					</div>
				</div>
				<div class="cta_remaining">
					Remaining Time To Place Bet
				</div>
			</div>
			<div class="title_cont">
				<p class="yellow_text_cta"><?=$title?></p>
				<p class="hurry_text_cta">Hurry up! 25 people have placed this bet</p>
			</div>
		</div>
		<div class="button_cta">
			<div class="cont_button">
				<a href="" class="actual_button">
					BET & WIN
				</a>
			</div>
			<div class="cta_sometext">
				Trusted<br>
				Sportsbetting.ag
			</div>
		</div>
		<script>
			var countDownDate = new Date("<?=$remaining?>").getTime();
			var x = setInterval(function() {
		    var now = new Date().getTime();
		    var distance = countDownDate - now;
		    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
		    document.getElementById("<?=$days?>").innerHTML = days;
		    document.getElementById("<?=$hours?>").innerHTML = hours;
		    document.getElementById("<?=$minutes?>").innerHTML = minutes;
		    document.getElementById("<?=$seconds?>").innerHTML = seconds;
		    if (distance < 0) {
		        clearInterval(x);
		        document.getElementById("<?=$days?>").innerHTML = 0;
			    document.getElementById("<?=$hours?>").innerHTML = 0;
			    document.getElementById("<?=$minutes?>").innerHTML = 0;
			    document.getElementById("<?=$seconds?>").innerHTML = 0;
		    }
			}, 1000);
		</script>
	</div>
	<?php
	$html = ob_get_contents(); 
	ob_end_clean();
	return $html;
}
