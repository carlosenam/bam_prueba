<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bam_prueba');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'tq^z;3dp#qIVmWC<<mRjk[7-dcwgL#M$fYn%{ @8r4>^%W|5Vl>n+~4`qG:gs&s(');
define('SECURE_AUTH_KEY',  'JZz=8G:Um@}BuWX+Bg]`xV4j>;~d=xQXs^JJPA`[,qp?M>BW-@0z:u6JO!Ga][b]');
define('LOGGED_IN_KEY',    '[fF;;-a:I;w[4sb50!}W;A,a1NW>TTC{%S/oc?Cywyi,=uV<a>L;fu]odJC+(lRG');
define('NONCE_KEY',        'cL!]W!!j72Ny=G~mqb//h*ddMw,%Jk(xnqnQRg8*{5L0|s0CWGF{sOo8.^SMg,4g');
define('AUTH_SALT',        ' jIr6ic3++[fSE(;JOWj1V4RifI5tl<0C}+sqmt^<]6bz3f/;h,1mg*[ql4f3m$C');
define('SECURE_AUTH_SALT', 'l%IlK7(ssOt;~2U}s5sHxC[l*&qEF</=-a.;JdL]|D(k={Y8pB{ty%02Klqv[9&x');
define('LOGGED_IN_SALT',   '1 ?$xTQ)A[5iMRw<}$}GFWwAPN#vn6BsEzPB@-EpkBZojC3L@%fZz),LAKvLA+(?');
define('NONCE_SALT',       'AQH~_bi!sk2cWWV~~{6)}>%lMZ{zgfe0o0; SwF1+^L.]^6X|VL;DGDorm$Zu9(i');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
